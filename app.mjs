// --> import libraries 
import express from 'express';

import bodyParser from 'body-parser';
import morgan 	  from 'morgan';
import { engine }  	from 'express-handlebars';

import USERS_MOCK from './data/mocks/MOCK_USERS.json';
// const USERS_MOCK = require('./data/mocks/MOCK_USERS.json')

// --> setting top-level variables
const app = express()

 // --> configuring application middleware 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(morgan('dev'))

// --> configuring handlebars middleware
const hbs_options = {
	extname	: '.hbs',
	layoutsDir 	: 'views/layouts/',
	partialsDir 	: 'views/partials/',
	defaultLayout	: 'main'
}
app.engine('hbs', engine(hbs_options));
app.set('view engine', 'hbs');
app.use(express.static('./public'))

// --> setting up routes
app.get('/', (req, res) => {
  res.render('homepage')
});
app.get('/users', (req, res) => {
    res.render('users_list',{users_arr:USERS_MOCK.users})
});

// --> starting up the application
app.listen(3000, ()=> {
  console.log('Express server listening on port 3000')
});